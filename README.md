# BliepIOSAPI Client Library

## Dependencies ##

+ iOS 7+
+ AFNetworking Library

## Setup ##

Use CocoaPods to manage the library by adding it to a project's `Podfile` and running:

    pod install

## Usage ##

Using the API requires the initialization of a `BliepClient` singleton with your consumer app's `client_id` and `client_secret`.

    BliepClient *client = [BliepClient initWithId:myAppClientId andSecret:myAppClientSecret];

Once the singleton has been initialized, it can thereafter be obtained using a parameterless helper since the credentials have already been stored, e.g.

    BliepClient *client = [BliepClient sharedClient];
    
Before the client can make API requests it requires an OAuth2 access token. This token can be obtained through the `authenticateClientCredentials` (for anonymous users) or the `authenticateResourceOwner` (for registered users) method, e.g.
    
    [client authenticateClientCredentials:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"JSON: %@", error);
    }];
    
The client exposes a `isNetworkReachable` method that returns true if the device is connected to a WWAN or WiFi network, e.g.

    BOOL canMakeAPIRequests = [client isNetworkReachable];

The client also provides a pair of convenience methods for determining if an access token is available.

    BOOL hasUserAccessToken = [client isResourceOwnerAuthorized];
    BOOL hasClientAccessToken = [client isClientAuthorized];
    
In order to avoid checking every response for successful authentication, the client exposes an `oauthError` block property that can be assigned to handle general invalid access token errors and notify your code to perform the appropriate cleanup and reauthentication, e.g.

    [BliepClient sharedClient].oauthError = ^(NSURLSessionDataTask *task, NSError *error) {
        [[BliepClient sharedClient] deauthenticateClient];
        [[BliepClient sharedClient] deauthenticateResourceOwner];
        [[BliepClient sharedClient] authenticateClientCredentials:nil failure:nil];
    };

## Response ##

Every API response returns a JSON object in a standard response envelope. This contains, at minimum, a `status` field for the request status code, and a `data` field with either a JSON object or array. In the case of paginated data, the response will also contain a `limit` field indicating how many objects can be returned, and a boolean `more` field, which indicates if there are additional pages of data available.

Example:

    {"status": "success", "data": {...} }
    
Example with paginated data:

    {"status": "success", "limit": 25, "more": true, "data": [...] }

## SSL ##

When developing with the API it may be useful to call a server with a self-signed SSL certificate. The `allowInvalidCertificates` method enables the client to accept untrusted certificates by setting the client's `AFSecurityPolicy`.

    [client allowInvalidCertificates];

Nota Bene: accepting untrusted certificates is a major security risk and should only be used for testing purposes, never in production!

## Building ##

The library supports builds for arm7 and and arm7s architectures. The netcode uses the latest `NSURL` and related classes, and so builds must target iOS 7 or above.

## Methods ##

The client wraps the HTTP-based *bliep API and exposes methods necessary to build user applications. The following methods are supported:

### authenticateClientCredentials ###

This method authenticates the OAuth consumer (your client) and returns an access token that can be used when making calls that don't require user authentication (e.g. registration, FAQ, etc.).

    [client authenticateClientCredentials:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"JSON: %@", error);
    }];

###### Parameters
  
None (the client id and secret are provided during initialization).

###### Response

    {"status": "success", "data": {
        "access_token": "4P71y6WYsJNAX2yWqm7yUiyyWXYJgShUi4ZMIF5Q", 
        "token_type": "access", 
        "expires_in": 31536000, 
        "refresh_token": "2ndNKyGh1qMqi5Tmpn7OpwyEumbfAmdMdF4w0b8R"
    }}

### createAccount ###

This method creates a new *bliep user account for a user with an existing SIM and associates the user with the given MSISDN.

    [client createAccount:@"myname" email:@"bliepuser@example.com" password:@"lajwi230g" sex:"M" birthdate:[NSDate dateWithString:@"2001-30-1 00:00:00 +0000"] msisdn:12345678901" success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"JSON: %@", error);
    }];

Note, after calling createAccount a confirmation code will be sent to the user via SMS. To complete the registration, the user will need to provide their code to the `confirmRegistration` method.

###### Parameters

+ email (required): a valid, previously unregistered email address.
+ sex (optional): gender, can be "M" or "F"
+ birthdate (optional): a valid birth date
+ name (required): username in *bliep
+ msisdn (required): the phone number for which a user is registering an account
+ password (required): user *bliep login password

###### Response

    {"status": "success", "data": {
        "name": "myname"
        "msisdn": "12345678901",
        "email": "12345678901",
        "sex": "M"
    }}

### confirmRegistration ###

This method completes a new account registration by supplying a code sent to the registered MSISDN via SMS. A successful response returns a user access token that can be used for authentication.

    [client confirmRegistration:@"2q08g0j3" success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"JSON: %@", error);
    }];

###### Parameters

+ msisdn (required): the same msisdn provided to the `createAccount` method
+ confirmcode (required): a registration confirmation code

###### Response

    {"status": "success", "data": {
        "access_token": "4P71y6WYsJNAX2yWqm7yUiyyWXYJgShUi4ZMIF5Q", 
        "token_type": "access", 
        "expires_in": 31536000, 
        "refresh_token": "2ndNKyGh1qMqi5Tmpn7OpwyEumbfAmdMdF4w0b8R"
    }}
    
### forgotLogin ###

This method sends the user a message with instructions on how to reset a forgotten password. One of either an email address or msisdn phone number must be provided.

    [client forgotLogin:@"bliep@example.com" msisdn:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"JSON: %@", error);
    }];

###### Parameters

+ email (required unless msisdn): the user's registered email address
+ msisdn (required unless email): the user's registered msisdn phone number

###### Response

    {"status": "success", "data": null}

### listPortingProviders ###

This method retrieves a list of possible porting providers available for *bliep number porting requests.

    [client listPortingProviders:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"JSON: %@", error);
    }];
    
Note, the response contains a `main_providers` field for convenience.

###### Parameters

None.

###### Response

    {"status": "success", "data": {
        "is_port_in_allowed": "true",
        "main_providers": [
            {"code": "BEN-BEN", "name": "T-Mobile"}, 
            {"code": "GSM1-SPM", "name": "KPN"} ],
        "providers": [
            {"code": "GSM1-SPVM", "name": "Rabo Mobiel"},
            {"code": "ETMB-UWIS", "name": "U-Wiss Telecom"} ]
    }}

### createPortIn ###

This method creates a number porting request from a given provider.

    [client createPortIn:"T-Mobile" iccid:"8931440300470910761" msisdn:"1234511231" contractType:1 success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"JSON: %@", error);
    }];

###### Parameters

+ provider (required): an ID if one of the providers listed in `listPortingProviders`
+ iccid (required): the user's iccid number
+ msisdn (required): the phone number for which a user is requesting a port-in
+ contract (required): can either be "PREPAID" or "POSTPAID"

###### Response

    {"status": "success", "data": {
        "donor_msisdn": "1234511231",
        "donor_iccid": "8931440300470910761", 
        "donor_contract_type": "POSTPAID",
        "donor_operator": "T-Mobile"}}

### listCreditHistory ###

This method lists a "paginated" credit history of the user's account.

    [client listCreditHistory:10 offset:0 success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"JSON: %@", error);
    }];

###### Parameters

+ limit (optional): the number of credit history items to return
+ skip (optional): the number of credit history items to skip

###### Response

    {"status": "success", "limit": 10, "more": true, "data": {
        "current_saldo": "8217,00",
        "items": [
            {"date": 1382134031000, "amount": "-2,00", "description": "10 MB internet in EU"},
            {"date": 1382107433000, "amount": "-10,00", "description": "aan vriend(in) gegeven"}
        ]
    }}

### shareCredit ###

This method allows the user to share their *bliep credit with another user.

    [client shareCredit:5 addressbook:"1234511231" msisdn:"1234511231" personalMessage:"enjoy, my friend" success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"JSON: %@", error);
    }];

###### Parameters

+ amount (required): the amount of credit to share
+ msisdn (optional): the number of the user with whom to share, required if addressbook is omitted
+ addressbook (optional): the name of the user with whom to share, required if msisdn is omitted

###### Response

    {"status": "success", "data": null}

### listSharingAddressBook ###

This method lists a user's friends with whom they have previously shared credit.

    [client listSharingAddressBook:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"JSON: %@", error);
    }];

###### Parameters

None.

###### Response

    {"status": "success", "data": {
        "friends": [
            {name:'Joost', msisdn:'1234511231'}, 
            {name:'Damiet', msisdn:'1947788230'}
        ]
    }}

### topupCredit ###

This method allows a user to add more credit to their account via a payment gateway. Submitting a topup request creates an order which must then be fullfilled via a web-based merchant gateway. If the `recurring` parameter is provided, then the topup will automatically occur based on the given condition.

    [client topupCredit:5 redirectURL:"mycustomurlscheeme://topup" recurring:@"lowcredit" success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"JSON: %@", error);
    }];

###### Parameters

+ amount (required): the amount of credit to add
+ redirectURL (required): the return URL to which the user is redirected after completing/canceling payment
+ recurring (optional): whether to subscribe the user to recurring payments; can either be NULL, "monthly" or "lowcredit"

###### Response

    {"status": "success", "data": {
        "redirect_url": "https://example.com/merchant/pay.html?countryCode=NL&allowedMethods=ideal"
    }}

### topupVoucherCredit ###

This method allows a user to add more credit to their account via a voucher code. After submitting the credit is immediately available to the user.

    [client topupVoucherCredit:@"3jfgj2320f23f2ffqqxq43t01dp" success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"JSON: %@", error);
    }];

###### Parameters

+ amount (required): the amount of credit to add

###### Response

    {"status": "success", "data": null}
    
### listCallHistory ###

This method lists a "paginated" call history for the user.

    [client listCallHistory:10 offset:0 success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"JSON: %@", error);
    }];

###### Parameters

+ limit (optional): the number of call history items to return
+ skip (optional): the number of call history items to skip

###### Response

    {"status": "success", "limit": 10, "more": true, "data": {
        "current_saldo": {"seconds": "00", "minutes": 3405},
        "items": [
            {"date": 1366805246000, "seconds": "600.000000", "description": "Topup"}, 
            {"date": 1366805232000, "seconds": "600.000000", "description": "Topup"}
        ]
    }}

### viewSettings ###

This method returns the user's general *bliep service settings.

    [client viewSettings:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"JSON: %@", error);
    }];

###### Parameters

None.

###### Response

    {"status": "success", "data": {
        "pukcode": "0001",
        "voicemail_active": true, 
        "call_from_balance": true, 
        "low_balance_notification": false
    }}
    
### updateSettings ###

This method updates the user's general *bliep service settings.

    [client updateSettings:YES voicemailActive:NO lowBalanceNotification:NO success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"JSON: %@", error);
    }];

###### Parameters

+ callFromBalance (required): toggles if the user's calling is enabled
+ voicemailActive (required): toggles if the user's voicemail is enabled
+ lowBalanceNotification (required): toggles if the recieves an SMS when their credit is low

###### Response

    {"status": "success", "data": {
        "voicemail_active": true, 
        "call_from_balance": false, 
        "low_balance_notification": false
    }}

### viewRoamingSettings ###
  
This methods returns the state of the authenticated user's roaming bundles.

    [client viewRoamingSettings:success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"JSON: %@", error);
    }]
  
###### Parameters
  
None.  
  
###### Response

    {"status": "success", "data": {
        "free_eu_internet": false, 
        "free_eu_use": false,
        "week_internet_bundle": true, 
        "day_internet_bundle": true, 
        "sms_bundle": true, 
        "balance": "8217,00"
    }}


### updateRoamingSettings ###

This method updates the user's roaming bundle settings.

    [client updateSettings:YES freeEUGPRSUse:NO dayInternetBundle:YES weekInternetBundle:NO roamingSmsBundle:NO success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"JSON: %@", error);
    }];

###### Parameters

+ freeEUUse (required): toggles if the user's EU bundle is enabled
+ freeEUGPRSUse (required): toggles if the user's GPRS is enabled
+ dayInternetBundle (required): toggles if the user's daily data bundle is enabled
+ weekInternetBundle (required): toggles if the user's weekly data bundle is enabled
+ roamingSmsBundle (required): toggles if the user's roaming SMS bundle is enabled

###### Response

    {"status": "success", "data": null}

### updateSimCardState ###

This method enables/disables a user's voice and data settings.

    [client updateSimCardState:YES plusOn:NO voiceOn:NO success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"JSON: %@", error);
    }];

###### Parameters

+ serviceOn (required):  a boolean toggling if the user's primary service bundle is turned on
+ plusOn (required): a boolean toggling if the user's plus bundle is turned on
+ voiceOn (required): a boolean toggling if the user's voice bundle is turned on

###### Response

    {"status": "success", "data": null}

### viewProfile ###

This method retrieves a user's primary information and account status details.

    [client viewProfile:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"JSON: %@", error);
    }];

###### Parameters

None

###### Response

    {"status": "success", "data": {
        "email": "example@bliep.nl",
        "msisdn": "00930005611",
        "name": "Jochem",
      "referral":"https://www.bliep.nl/r/0001099c4100",
        "calltime": {"seconds": 0, "minutes": 3405}, 
        "has_running_task": false, 
        "member_id": 1, 
        "state": "voice", 
        "signature": "bliep", 
        "balance": "8217.00", 
        "tariff": {"on": true, "voice": true, "plus": false}, 
        "today_paid_for": {"on": true, "voice": false, "plus": false}
    }}

### viewReferral ###

This method retrieves the user's referral link used for "spread the *bliep" credit-earning.

    [client viewReferral:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"JSON: %@", error);
    }];

###### Parameters

None.

###### Response

    {"status": "success", "data": {
        'clicks':32,
        'credit':'12,50',
        'referral':'https://www.bliep.nl/r/1b9ad21c4100'
    }}

### viewSignature ###

This method retrieves the user's signature and signature display preference.

    [client viewSignature:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"JSON: %@", error);
    }];

###### Parameters

None

###### Response

    {"status": "success", "data": {
        'signature': '*yolo',
        'use': 'use_custom'
    }}

The `use` property is an enumeration containing one of three possible values:

1. use_custom: append the user's custom signature string
2. use_bliep: append the default "*bliep" signature
3. use_none: do not append a signature

### updateSignature ###

This method updates the user's signature and/or signature display preference.

###### Parameters
  
    {"signature": "c'est moi", "use": "use_custom"}

+ signature (required): the alphanumeric signature text.
+ use (optional): enable or disable the signature in SMS.  

The `use` parameter can contain one of three possible values:

1. use_custom: append the user's custom signature string
2. use_bliep: append the default "*bliep" signature
3. use_none: do not append a signature

###### Response

    {"status": "success", "data": null}
    
### updateProfile ###

This method updates the user's email and/or password. The old password and email act as on-demand authorization for any updates to the user's login credentials.

###### Parameters
  
    {"email": "bliep@example.com", "password": 'mypass123', "new_password": 'newpass123'}

+ email (required): the user's existing email address if updating password or new address if updating email.
+ password (required): the user's current password
+ new_password (optional): the user's new password

###### Response

    {"status": "success", "data": null}

### listFAQ ###

This method lists the *bliep FAQ either by category or (by default) currently highlighted.

    [client listFAQ:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"JSON: %@", error);
    }];

###### Parameters

+ category_id (optional): the FAQ category by which to filter the results.

###### Response

    {"status": "success", "data": {"faqs": [
        {"category": 2, "answer": "Log in en klik...", "question": "Hoe zet ik *bliep aan?", "id": 1, "keywords": "service"},
        {"category": 3, "answer": "Log in en klik...", "question": "Hoe zet ik mijn nummer over?", "id": 2, "keywords": "porting"},
        
    ]}}

### searchFAQ ###

This method returns FAQ matching the given keyword search input. Note, the search itself is quite literal and doesn't support wildcards.

    [client searchFAQ:@"number porting" success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"JSON: %@", error);
    }];

###### Parameters

+ text (required): an alphanumeric string of keywords to find in the FAQ.

###### Response

    {"status": "success", "data": {"faqs": [
        {"category": 2, "answer": "Log in en klik...", "question": "Hoe zet ik *bliep aan?", "id": 1, "keywords": "service"},
        {"category": 3, "answer": "Log in en klik...", "question": "Hoe zet ik mijn nummer over?", "id": 2, "keywords": "porting"},
        
    ]}}  
    
#### askQuestion ####

This method submits a question to the *bliep support team using a given email.

    [client searchFAQ:@"jochem" email:@"jochem@example.com" message:@"Is the *bliep iOS API public?" success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"JSON: %@", error);
    }];

###### Parameters

+ name (required): the name of the person asking the question
+ email (required): a valid email address for the person asking a question; used for follow-up coorespondence
+ message (required): a plaintext message containing a question.

###### Response

    {"status": "success", "data": null }}