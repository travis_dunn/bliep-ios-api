//
//  AFOAuthCredentials.h
//  BliepIOSAPI
//
//  Created by Travis Dunn on 10/3/13.
//  Copyright (c) 2013 *bliep. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface AFOAuthCredentials : NSObject <NSCoding>

@property (strong, nonatomic) NSString *accessToken;
@property (strong, nonatomic) NSString *tokenType;
@property (strong, nonatomic) NSString *refreshToken;
@property (strong, nonatomic) NSDate *expiration;

+ (AFOAuthCredentials *)retrieveCredentialsWithIdentifier:(NSString *)identifier;

+ (BOOL)storeCredentials:(AFOAuthCredentials *)credential
          withIdentifier:(NSString *)identifier;

+ (BOOL)deleteCredentialsWithIdentifier:(NSString *)identifier;

- (BOOL)isValid;

@end
