//
//  AFAPIClient.m
//  BliepIOSAPI
//
//  Created by Travis Dunn on 10/29/13.
//  Copyright (c) 2013 *bliep. All rights reserved.
//

#import "AFAPIClient.h"

@implementation AFAPIClient

- (void) allowInvalidCertificates {
    self.securityPolicy.allowInvalidCertificates = YES;
}

- (BOOL) isNetworkReachable {
    return [[AFNetworkReachabilityManager sharedManager] isReachable];
}

- (void) setReachabilityStatusChanged:(void (^)(BOOL isConnected))block {
    [self.reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        BOOL isConnected = (status == AFNetworkReachabilityStatusReachableViaWWAN ||
                            status == AFNetworkReachabilityStatusReachableViaWiFi);
        if (block) block(isConnected);
    }];
}

- (void) resetReachability {
    [self.reachabilityManager setReachabilityStatusChangeBlock:nil];
}

- (NSURLSessionDataTask *)GET:(NSString *)URLString
                   parameters:(NSDictionary *)parameters
                      success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                      failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    // wrap http method with standardized api error handling
    return [super GET:URLString parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        [self respond:task withData:responseObject success:success failure:failure];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        if (failure) failure(task, error);
    }];
}

- (NSURLSessionDataTask *)PUT:(NSString *)URLString
                   parameters:(NSDictionary *)parameters
                      success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                      failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    // wrap http method with standardized api error handling
    return [super PUT:URLString parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        [self respond:task withData:responseObject success:success failure:failure];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        if (failure) failure(task, error);
    }];
}

- (NSURLSessionDataTask *)POST:(NSString *)URLString
                    parameters:(NSDictionary *)parameters
                       success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                       failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    // wrap http method with standardized api error handling
    return [super POST:URLString parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        [self respond:task withData:responseObject success:success failure:failure];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        if (failure) failure(task, error);
    }];
}

- (void) respond:(NSURLSessionDataTask *)task
        withData:(id)data
         success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
         failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    NSString *status = [data valueForKey:@"status"];
    if ([status isEqualToString:@"error"] || [status isEqualToString:@"fail"]) {
        if (failure) failure(task, [self getError:data]);
    } else if ([data valueForKey:@"error"]) {
        if (failure) failure(task, [self getError:data]);
    } else {
        if (success) success(task, data);
    }
}

- (NSError *)getError:(NSDictionary *)responseData {
    id message = [responseData valueForKey:@"message"];
    if (!message) [responseData valueForKey:@"error"];
    
    if ([message isKindOfClass:[NSDictionary class]]) {
        NSArray *values = [(NSDictionary *)message allValues];
        if ([values count] == 0) {
            message = @"An error occurred";
        } else {
            message = [values objectAtIndex:0];
        }
    }
    
    if (!message) message = @"An error occurred";
    
    NSDictionary *details = [NSDictionary dictionaryWithObjectsAndKeys:message, NSLocalizedDescriptionKey, message, NSLocalizedFailureReasonErrorKey, nil];
    NSError *error = [NSError errorWithDomain:message code:0 userInfo:details];
    return error;
}

@end
