//
//  AFOAuthCredentials.m
//  BliepIOSAPI
//
//  Created by Travis Dunn on 10/3/13.
//  Copyright (c) 2013 *bliep. All rights reserved.
//

#import "AFOAuthCredentials.h"
#import "UICKeyChainStore.h"

@interface AFOAuthCredentials()

+ (UICKeyChainStore *)storeWithIdentifier:(NSString *)identifier;

@end

@implementation AFOAuthCredentials

@synthesize accessToken;
@synthesize tokenType;
@synthesize refreshToken;
@synthesize expiration;

+ (BOOL)storeCredentials:(AFOAuthCredentials *)credential withIdentifier:(NSString *)identifier {
    UICKeyChainStore *store = [self storeWithIdentifier:identifier];
    [store setString:credential.accessToken forKey:@"accessToken"];
    [store setString:credential.refreshToken forKey:@"refreshToken"];
    [store setString:credential.tokenType forKey:@"tokenType"];
    
    [store synchronize];
    return true;
}

+ (AFOAuthCredentials *)retrieveCredentialsWithIdentifier:(NSString *)identifier {
    UICKeyChainStore *store = [self storeWithIdentifier:identifier];
    AFOAuthCredentials *credentials = [[AFOAuthCredentials alloc] init];
    credentials.accessToken = [store stringForKey:@"accessToken"];
    credentials.refreshToken = [store stringForKey:@"refreshToken"];
    credentials.tokenType = [store stringForKey:@"tokenType"];
    
    return credentials;
}

+ (BOOL)deleteCredentialsWithIdentifier:(NSString *)identifier {
    return [UICKeyChainStore removeAllItemsForService:[@"bliep.oauth." stringByAppendingString:identifier]];
}

+ (UICKeyChainStore *)storeWithIdentifier:(NSString *)identifier {
    return [UICKeyChainStore keyChainStoreWithService:[@"bliep.oauth." stringByAppendingString:identifier]];
}

- (BOOL)isValid {
    return self.accessToken != nil && ![self.accessToken isEqual:[NSNull null]];
}

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    self.accessToken = [decoder decodeObjectForKey:@"accessToken"];
    self.tokenType = [decoder decodeObjectForKey:@"tokenType"];
    self.refreshToken = [decoder decodeObjectForKey:@"refreshToken"];
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.accessToken forKey:@"accessToken"];
    [encoder encodeObject:self.tokenType forKey:@"tokenType"];
    [encoder encodeObject:self.refreshToken forKey:@"refreshToken"];
}

@end
