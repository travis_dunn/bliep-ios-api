//
//  AFOAuth2Client.h
//  BliepIOSAPI
//
//  Created by Travis Dunn on 10/3/13.
//  Copyright (c) 2013 *bliep. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPSessionManager.h"
#import "AFAPIClient.h"

typedef void (^OAuth2ErrorBlock)(NSURLSessionDataTask *task, NSError *error);

@class AFOAuthCredentials;

@interface AFOAuth2Client : AFAPIClient

@property (strong, nonatomic) NSString *clientId;
@property (strong, nonatomic) NSString *clientSecret;
@property (strong, nonatomic) NSString *redirectUri;

@property(readwrite, copy) OAuth2ErrorBlock oauthError;

- (void)authenticateResourceOwner:(NSString *)username
                         password:(NSString *)password
                          success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                          failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (void)authenticateClientCredentials:
(void (^)(NSURLSessionDataTask *task, id responseObject))success
                              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (void)authenticateRefreshToken:(NSString *)refreshToken
                         success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                         failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (void)deauthenticateResourceOwner;
- (void)deauthenticateClient;

- (BOOL) isResourceOwnerAuthorized;
- (BOOL) isClientAuthorized;

- (AFOAuthCredentials *) getCredentials;
- (void)setAuthorizationHeaderWithCredentials:(AFOAuthCredentials *) credentials;

- (NSURLSessionDataTask *)GET:(NSString *)URLString
                   parameters:(NSDictionary *)parameters
                      success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                      failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)PUT:(NSString *)URLString
                   parameters:(NSDictionary *)parameters
                      success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                      failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)POST:(NSString *)URLString
                    parameters:(NSDictionary *)parameters
                       success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                       failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

@end







