//
//  Porting.m
//  BliepIOSAPI
//
//  Created by Travis Dunn on 10/3/13.
//  Copyright (c) 2013 *bliep. All rights reserved.
//

#import "Porting.h"
#import "BliepClient.h"

@implementation Porting

+ (NSURLSessionDataTask *)listPortingProviders:(BliepClient *)client
                                       success: (void (^)(NSURLSessionDataTask *task, id responseObject))success
                                       failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    return [client GET:@"porting/providers" parameters:NULL success:success failure:failure];
}

+ (NSURLSessionDataTask *)createPortIn:(BliepClient *)client
                              provider:(NSString *)provider
                                 iccid:(NSString *)iccid
                                msisdn:(NSString *)msisdn
                          contractType:(NSNumber *)contract
                               success: (void (^)(NSURLSessionDataTask *task, id responseObject))success
                               failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                provider, @"provider",
                                iccid ? iccid : [NSNull null], @"iccid",
                                msisdn ? msisdn : [NSNull null], @"msisdn",
                                contract, @"contract", nil];
    
    return [client POST:@"porting" parameters:parameters success:success failure:failure];
}
@end
