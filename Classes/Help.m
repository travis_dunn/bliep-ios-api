//
//  Help.m
//  BliepIOSAPI
//
//  Created by Travis Dunn on 10/21/13.
//  Copyright (c) 2013 *bliep. All rights reserved.
//

#import "Help.h"
#import "BliepClient.h"

@implementation Help

+ (NSURLSessionDataTask *)listFAQ:(BliepClient *)client
                         category:(NSInteger)categoryId
                          success: (void (^)(NSURLSessionDataTask *task, id responseObject))success
                          failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    NSDictionary *parameters = [NSDictionary dictionaryWithObject:[NSNumber numberWithInteger:categoryId]
                                                           forKey:@"category"];
    
    return [client GET:@"help/faq" parameters:parameters success:success failure:failure];
}

+ (NSURLSessionDataTask *)searchFAQ:(BliepClient *)client
                               text:(NSString *)text
                            success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                            failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    NSDictionary *parameters = [NSDictionary dictionaryWithObject:text forKey:@"text"];
    
    return [client GET:@"help/search" parameters:parameters success:success failure:failure];
}

+ (NSURLSessionDataTask *)askQuestion:(BliepClient *)client
                                 name:(NSString *)name
                                email:(NSString *)email
                              message:(NSString *)message
                              success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                name ? name : [NSNull null], @"name",
                                email ? email : [NSNull null], @"email",
                                message ? message : [NSNull null], @"message", nil];
    
    return [client POST:@"help/faq" parameters:parameters success:success failure:failure];
    
}


@end
