//
//  Alerts.h
//  BliepIPhoneApi
//
//  Created by Travis Dunn on 18/11/14.
//  Copyright (c) 2014 *bliep. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BliepClient;

@interface Alerts : NSObject

+ (NSURLSessionDataTask *)viewAlertMessage:(BliepClient *)client
                                   success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                   failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

@end
