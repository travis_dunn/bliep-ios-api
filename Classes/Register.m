//
//  Register.m
//  BliepIOSAPI
//
//  Created by Travis Dunn on 10/17/13.
//  Copyright (c) 2013 *bliep. All rights reserved.
//

#import "Register.h"
#import "BliepClient.h"

@implementation Register

+ (NSURLSessionDataTask *)createAccount:(BliepClient *)client
                               username:(NSString *)name
                                  email:(NSString *)email
                               password:(NSString *)password
                                    sex:(NSString *)sex
                              birthdate:(NSDate *)birthdate
                                 msisdn:(NSString *)msisdn
                                success: (void (^)(NSURLSessionDataTask *task, id responseObject))success
                                failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                name, @"name",
                                email, @"email",
                                password, @"password",
                                msisdn, @"msisdn",
                                birthdate ? birthdate : [NSNull null], @"birthdate",
                                sex ? sex : [NSNull null], @"sex",
                                nil];
    
    return [client POST:@"registration" parameters:parameters success:success failure:failure];
}

+ (NSURLSessionDataTask *)confirmAccount:(BliepClient *)client
                        confirmationCode:(NSString *)code
                                  msisdn:(NSString *)msisdn
                                 success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                 failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                code, @"confirmcode",
                                msisdn, @"msisdn", nil];
    
    return [client PUT:@"registration" parameters:parameters success:success failure:failure];
}

+ (NSURLSessionDataTask *)forgotLogin:(BliepClient *)client
                                email:(NSString *)email
                               msisdn:(NSString *)msisdn
                              success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                email ? email : [NSNull null], @"email",
                                msisdn ? msisdn : [NSNull null], @"msisdn", nil];
    
    return [client POST:@"profile/forgot_login" parameters:parameters success:success failure:failure];
}

@end

