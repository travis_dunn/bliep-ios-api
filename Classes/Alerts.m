//
//  Alerts.m
//  BliepIPhoneApi
//
//  Created by Travis Dunn on 18/11/14.
//  Copyright (c) 2014 *bliep. All rights reserved.
//

#import "Alerts.h"
#import "BliepClient.h"

@implementation Alerts

+ (NSURLSessionDataTask *)viewAlertMessage:(BliepClient *)client
                                   success: (void (^)(NSURLSessionDataTask *task, id responseObject))success
                                   failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    return [client GET:@"message" parameters:NULL success:success failure:failure];
}

@end
