//
//  Profile.m
//  BliepIOSAPI
//
//  Created by Travis Dunn on 10/3/13.
//  Copyright (c) 2013 *bliep. All rights reserved.
//

#import "Profile.h"
#import "BliepClient.h"

@implementation Profile

+ (NSURLSessionDataTask *)viewProfile:(BliepClient *)client
                              success: (void (^)(NSURLSessionDataTask *task, id responseObject))success
                              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    return [client GET:@"profile" parameters:NULL success:success failure:failure];
}

+ (NSURLSessionDataTask *)viewReferral:(BliepClient *)client
                               success: (void (^)(NSURLSessionDataTask *task, id responseObject))success
                               failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    return [client GET:@"profile/referral" parameters:NULL success:success failure:failure];
}

+ (NSURLSessionDataTask *)viewSignature:(BliepClient *)client
                                success: (void (^)(NSURLSessionDataTask *task, id responseObject))success
                                failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    return [client GET:@"profile/signature" parameters:NULL success:success failure:failure];
}

+ (NSURLSessionDataTask *)updateSignature:(BliepClient *)client
                                signature:(NSString *)signature
                                      use:(NSString *)use
                                  success: (void (^)(NSURLSessionDataTask *task, id responseObject))success
                                  failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                use ? use : @"use_none", @"use",
                                signature ? signature : [NSNull null], @"signature",
                                nil];
    
    return [client PUT:@"profile/signature" parameters:parameters success:success failure:failure];
}

+ (NSURLSessionDataTask *)updateProfile:(BliepClient *)client
                                  email:(NSString *)email
                            oldPassword:(NSString *)oldPassword
                            newPassword:(NSString *)newPassword
                                success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                oldPassword, @"password",
                                newPassword ? newPassword : [NSNull null], @"new_password",
                                email ? email : [NSNull null], @"email",
                                nil];
    
    return [client PUT:@"profile" parameters:parameters success:success failure:failure];
}


@end
