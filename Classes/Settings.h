//
//  Settings.h
//  BliepIOSAPI
//
//  Created by Travis Dunn on 10/3/13.
//  Copyright (c) 2013 *bliep. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BliepClient;

@interface Settings : NSObject

+ (NSURLSessionDataTask *)viewSettings:(BliepClient *)client
                               success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                               failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


+ (NSURLSessionDataTask *)updateSettings:(BliepClient *)client
                         callFromBalance:(BOOL)callFromBalance
                         voicemailActive:(BOOL)voicemailActive
                  lowBalanceNotification:(BOOL)lowBalanceNotification
                                 success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                 failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+ (NSURLSessionDataTask *)viewRoamingSettings:(BliepClient *)client
                                      success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                      failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+ (NSURLSessionDataTask *)updateRoamingSettings:(BliepClient *)client
                                      freeEUUse:(BOOL)freeEUUse
                                  freeEUGPRSUse:(BOOL)freeEUGPRSUse
                              dayInternetBundle:(BOOL)dayInternetBundle
                             weekInternetBundle:(BOOL)weekInternetBundle
                               roamingSmsBundle:(BOOL)roamingSmsBundle
                                        success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                        failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+ (NSURLSessionDataTask *)viewPaymentSettings:(BliepClient *)client
                                      success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                      failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+ (NSURLSessionDataTask *)updatePaymentSettings:(BliepClient *)client
                                  enableMonthly:(BOOL)enableMonthly
                                  monthlyAmount:(NSNumber *)monthlyAmount
                                enableLowCredit:(BOOL)enableLowCredit
                                lowCreditAmount:(NSNumber *)lowCreditAmount
                                        success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                        failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+ (NSURLSessionDataTask *)disablePaymentSettings:(BliepClient *)client
                                         success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                         failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


@end
