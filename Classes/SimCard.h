//
//  SimCard.h
//  BliepIOSAPI
//
//  Created by Travis Dunn on 10/3/13.
//  Copyright (c) 2013 *bliep. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BliepClient;

@interface SimCard : NSObject

+ (NSURLSessionDataTask *)updateSimCardState:(BliepClient *)client
                                   serviceOn:(BOOL)serviceOn
                                      plusOn:(BOOL)plusOn
                                     voiceOn:(BOOL)voiceOn
                                     success: (void (^)(NSURLSessionDataTask *task, id responseObject))success
                                     failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


@end
