//
//  SimCard.m
//  BliepIOSAPI
//
//  Created by Travis Dunn on 10/3/13.
//  Copyright (c) 2013 *bliep. All rights reserved.
//

#import "SimCard.h"
#import "BliepClient.h"

@implementation SimCard

+ (NSURLSessionDataTask *)updateSimCardState:(BliepClient *)client
                                   serviceOn:(BOOL)serviceOn
                                      plusOn:(BOOL)plusOn
                                     voiceOn:(BOOL)voiceOn
                                     success: (void (^)(NSURLSessionDataTask *task, id responseObject))success
                                     failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                @(serviceOn), @"on",
                                @(plusOn), @"plus",
                                @(voiceOn), @"voice", nil];
    
    return [client PUT:@"profile/state" parameters:parameters success:success failure:failure];
}


@end
