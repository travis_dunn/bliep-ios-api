
Pod::Spec.new do |s|
  s.name         = "BliepIOSAPI"
  s.version      = "2.13"
  s.summary      = "OAuth consumer client SDK for the *bliep MVNO API."

  s.description  = <<-DESC
                   OAuth consumer client SDK for the *bliep MVNO API.
                   DESC

  s.homepage     = "https://www.bliep.nl"
  s.license      = { :type => 'MIT', :file => 'LICENSE.txt' }
  s.author       = { "Travis Dunn" => "travis@bliep.nl" }

  s.platform     = :ios, '6.0'
  s.source        = { :git => 'https://bitbucket.org/bliepteam/bliep-ios-api.git' }


  s.source_files  = 'Classes/**/*.{h,m}', 'OAuth2/**/*.{h,m}', '*.{h,m}'
  s.exclude_files = 'Classes/Exclude'

  s.dependency 'AFNetworking', '~> 2.5'
  s.dependency 'UICKeyChainStore', '~> 1.0.0'

end
