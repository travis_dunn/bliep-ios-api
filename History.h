//
//  History.h
//  BliepIOSAPI
//
//  Created by Travis Dunn on 11/05/15.
//  Copyright (c) 2015 *bliep. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BliepClient;

@interface History : NSObject

+ (NSURLSessionDataTask *)listHistory:(BliepClient *)client
                                    endDate:(NSDate *)date
                                  success: (void (^)(NSURLSessionDataTask *task, id responseObject))success
                              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

@end

